import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import logo from './logo.png';
import './App.css';
import algorithm from './API/Algorithm';
import {
  Restaurants,
} from './API/SampleData';
import Progressbar from './components/Progressbar';

interface componentState {
  result: string;
  day: number;
}

class App extends Component {
  state: componentState = {
    result: '',
    day: 0,
  }

  executeNextDay = () => {
    this.setState((prevState: componentState) => ({
      result: algorithm(),
      day: prevState.day + 1
    }));
  };

  render() {
    const {
      result,
    } = this.state;

    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Givun</h1>
          <h3>PoC!!</h3>
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Algorithm response (aka: Today you gonna eat): 
          </p>
          <p>
            {result}
          </p>
          <div className="container">
            <div className="row">
              <div className="col-md-12 col-sm-12 col-xs-12">            
                <button
                  type="button"
                  className="btn btn-warning align-self-center"
                  onClick={ this.executeNextDay }>
                  Next day >
                </button>
              </div>
              {
                Restaurants.map(({ id, name, numberOfVisits }) => (
                  <Progressbar
                    key={ id }
                    name={ name }
                    numberOfVisits={ numberOfVisits }
                  />
                ))
              }
            </div>
          </div>
        </header>
      </div>
    );
  }
}

export default App;
