export interface Restaurant {
    id: number;
    name: string;
    numberOfVisits: number;
};