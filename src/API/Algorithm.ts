import {
    Restaurant,
} from './Interfaces';
import {
    Restaurants,
} from './SampleData';

function totalVisits(restaurants: Array<Restaurant>): number {
    return restaurants.reduce((totalVisits, { numberOfVisits }) => totalVisits + numberOfVisits, 0);
}

function algorithm(restaurants: Array<Restaurant> = Restaurants): string {
    const tv = totalVisits(restaurants);
    const chances: Array<{
        id: number,
        name: string,
        chance: number,
    }> = [];

    const pickFromHere: Array<number> = [];

    for (let i = 0; i < restaurants.length; i++) {
        const id = restaurants[i].id;
        const chance = Math.ceil((1 / restaurants.length) * (100 - ((restaurants[i].numberOfVisits / tv) * 100 )));
        chances.push({
            id,
            name: restaurants[i].name,
            chance,
        });
        pickFromHere.push(...Array(chance).fill(id));
    }
    const pickedRestaurantID = pickFromHere[Math.floor(Math.random() * pickFromHere.length)];
    const pickedRestaurantIndex = Restaurants.findIndex(({ id }) => id === pickedRestaurantID);
    Restaurants[pickedRestaurantIndex].numberOfVisits ++;

    return Restaurants[pickedRestaurantIndex].name;
}

export const tv = totalVisits(Restaurants);

export default algorithm;
