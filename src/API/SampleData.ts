import {
    Restaurant
} from './Interfaces';

export const Restaurants: Array<Restaurant> = [
    {
        id: 0,
        name: 'McDonalds',
        numberOfVisits: 1,
    },
    {
        id: 1,
        name: 'Moses',
        numberOfVisits: 1,
    },
    {
        id: 2,
        name: 'Burger Market',
        numberOfVisits: 1,
    },
    {
        id: 3,
        name: 'Black Burger',
        numberOfVisits: 1,
    },
    {
        id: 4,
        name: 'Josef',
        numberOfVisits: 1,
    },
];
