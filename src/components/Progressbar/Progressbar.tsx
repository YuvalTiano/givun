import React from 'react';
import {
    tv,
} from '../../API/Algorithm';
import './Progressbar.css';

interface Props {
    name: string;
    numberOfVisits: number;
}

const Progressbar = ({
    name,
    numberOfVisits
}: Props) => {
    return (
        <div className="col-md-12 col-sm-12 col-xs-12 styled-progressbar">
            <div className="progress">
                <div
                    className="progress-bar progress-bar-striped bg-warning"
                    role="progressbar"
                    aria-valuemin={ 0 }
                    aria-valuemax={ tv }
                    aria-valuenow={ numberOfVisits }
                    style={{ width: `${numberOfVisits}%`}}
                    >
                    {`${name}: ${numberOfVisits}`}
                </div>
            </div>
        </div>
    );
};

export default Progressbar;
